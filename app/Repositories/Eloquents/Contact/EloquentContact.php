<?php
namespace App\Repositories\Eloquents\Contact;

use App\Models\Contact;
use App\Repositories\Eloquents\BaseEloquent;

class EloquentContact extends BaseEloquent implements ContactInterface
{
	private $model;

	public function __construct(Contact $model)
	{
		$this->model = $model;
		parent::__construct($model);
	}

	public function updateUserDetail(int $user_id, array $attribute)
	{
		return $this->model->where('user_id',$user_id)->update($attribute);
	}
}