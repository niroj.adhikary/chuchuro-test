<?php
namespace App\Repositories\Eloquents\Contact;

use App\Repositories\BaseInterface;

interface ContactInterface extends BaseInterface
{
	public function updateUserDetail(int $user_id, array $attribute);
}