<?php
namespace App\Repositories\Eloquents;

use App\Repositories\BaseInterface;

class BaseEloquent implements BaseInterface
{
	private $model;

	public function __construct($model)
	{
		$this->model = $model;
	}

	public function getAll(int $limit = null)
    {
        if($limit === null){
        	return $this->model->all();
        }
        return $this->model->paginate($limit);
    }

    public function findBy(string $field, $value)
    {
    	return $this->model->where($field, '=', $value)->first();
    }

    public function create(array $attributes)
	{
		return $this->model->create($attributes);
	}

	public function update(array $where, array $attributes)
	{
		return $this->model->where($where)->update($attributes);
	}
}