<?php
namespace App\Repositories\Eloquents\User;

use App\User;
use App\Repositories\Eloquents\BaseEloquent;

class EloquentUser extends BaseEloquent implements UserInterface
{
	private $model;

	public function __construct(User $model)
	{
		$this->model = $model;
		parent::__construct($model);
	}

	public function userExists(int $phone_number)
	{
		$existingUser = $this->model->where('phone_number',$phone_number)->count();
		if($existingUser === 1){
			return true;
		}
		return;
	}
}