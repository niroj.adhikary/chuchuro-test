<?php
namespace App\Repositories\Eloquents\User;

use App\Repositories\BaseInterface;

interface UserInterface extends BaseInterface
{
	public function userExists(int $phone_number);
}