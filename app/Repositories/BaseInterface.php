<?php
namespace App\Repositories;

interface BaseInterface
{

	public function getAll(int $limit = null);

	public function findBy(string $field, $value);

	public function create(array $attributes);

	public function update(array $where, array $attributes);
}
