<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Eloquents\User\UserInterface;
use App\Repositories\Eloquents\Contact\ContactInterface;
use Illuminate\Support\Str;

class apiImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from given api link';

    
    protected $user;

    protected $contact;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    
    public function __construct(UserInterface $user, ContactInterface $contact)
    {
        parent::__construct();
        $this->user = $user;
        $this->contact = $contact;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', env('API_URL'));
        if($res->getStatusCode() === 200){
            $response_results = json_decode($res->getBody());
            if(isset($response_results->contacts)){
                foreach($response_results->contacts as $contact){
                    $userExists = $this->user->userExists(removeSpecialCharacterFromPhone($contact->phone_number));
                    if($userExists === null){
                        $this->createNewUser($contact);
                    }else{
                        $this->updateExistingUser($contact);
                    }
                }
            }
            $this->info('Data imported successfully.');
        }else{
            //error
            $this->info('Server is not responding.');
        }
    }

    public function createNewUser($contact)
    {
        $generatedPassword = Str::random(15);
        $new_user = $this->user->create([
            'name'          =>  $contact->name,
            'phone_number'  =>  removeSpecialCharacterFromPhone($contact->phone_number),
            'password'      =>  bcrypt($generatedPassword)
        ]);
        //insert contact detail
        $this->contact->create([
            'user_id'       =>  $new_user['id'],
            'address'       =>  $contact->address
        ]);
    }

    public function updateExistingUser($contact)
    {
        $phone_number = removeSpecialCharacterFromPhone($contact->phone_number);
        $userDetail = $this->user->findBy('phone_number',$phone_number);
        $this->contact->update(['user_id' => $userDetail->id],[
            'address'       =>  $contact->address
        ]);
    }
}
