<?php
//remove + sign from contact no
if(!function_exists('removeSpecialCharacterFromPhone')){
	function removeSpecialCharacterFromPhone($phone_number){
		return preg_replace('/\W/','',$phone_number);
	}
}

//add + sign while displaying contact no
if(!function_exists('addPlusSignInPhone')){
	function addPlusSignInPhone($phone_number){
		return '+'.$phone_number;
	}
}
?>