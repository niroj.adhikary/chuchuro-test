<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $models = array(
            'User',
            'Contact'
        );

        foreach ($models as $model) {
            $this->app->bind(
                'App\Repositories\Eloquents\\'.$model.'\\'.$model.'Interface',
                'App\Repositories\Eloquents\\'.$model.'\Eloquent'.$model
            );
        }
    }
}
