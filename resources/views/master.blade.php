<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('partials.header')
<body>
    @include('partials.navigation')
    @yield('content')
    @include('partials.footer')
</body>
</html>
