@extends('master')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<h1 class="mt-5">Contact Lists</h1>
			<ul class="list-unstyled">
				@foreach($users as $user)
				<li style="border: 1px solid #ccc">
					<ul>
						<li>Name: {{ $user->name }}</li>
						<li>Phone Number: {{ addPlusSignInPhone($user->phone_number) }}</li>
						<li>Address: {{ $user->contact->address }}</li>
					</ul>
				</li>
				@endforeach
			</ul>
			{{ $users->links() }}
		</div>
	</div>
</div>
@endsection